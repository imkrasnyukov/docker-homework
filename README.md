# Docker Homework

Хочу максимальную оценку

The Dockerfile in **user-bot** dir starts with a base image of `python:3.10.10` and sets the working directory to `/usr/src/app`. 

It then copies the `requirements.txt` file to the working directory and installs some dependencies using `apt-get` package manager such as `gnupg` and `google-chrome-stable`.

The `requirements.txt` file is then used to install Python packages via `pip3`.

Next, the current directory (which includes the application code) is copied into the container.

Finally, the `CMD` instruction is used to specify the command to run when the container is started, which is `python ./main.py`.

---

The Dockerfile in **admin-bot** dir starts with a base image of `python:3.10.10` and sets the working directory to `/usr/src/app`.

It then copies the `requirements.txt` file to the working directory and installs the Python packages listed in `requirements.txt` using `pip`.

The current directory (which includes the application code) is then copied into the container.

Finally, the `CMD` instruction is used to specify the command to run when the container is started, which is `python ./main.py`.

---

The `docker-compose.yml` file uses version `3.9` of the Docker Compose syntax to define five services:

1. `user-bot`: This service is built from the `./user-bot` directory using the `build` directive. It has two dependencies (`user-db` and `rabbitmq`) and is set to always restart. It also has a shared memory size of 2GB using the `shm_size` directive.

2. `admin-bot`: This service is built from the `./admin-bot` directory using the `build` directive. It depends on `rabbitmq` and is set to always restart.

3. `rabbitmq`: This service uses the official RabbitMQ image from Docker Hub and is set to always restart. It has two environment variables (`RABBITMQ_DEFAULT_USER` and `RABBITMQ_DEFAULT_PASS`) and exposes port `15672` for the RabbitMQ management UI.

4. `user-db`: This service uses the official Postgres image from Docker Hub and is set to always restart. It has two environment variables (`POSTGRES_USER` and `POSTGRES_PASSWORD`) and exposes port `5432` for Postgres. It also creates a named volume `pgdata-user` for persistent data storage.

5. `dozzle`: This service runs a containerized log viewer for Docker logs using the `amir20/dozzle` image from Docker Hub. It exposes port `9999` and connects to the Docker daemon using the `/var/run/docker.sock` volume mount.

The `volumes` directive creates a named volume `pgdata-user` which is used by the `user-db` service for persistent data storage.


