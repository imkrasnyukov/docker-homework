from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import Message, ContentType, Document, CallbackQuery

from bot.keyboard import start_keyboard
from config.const import BOT_API_TOKEN, ADMIN_USERNAMES, USER_TABLE_FILE_PATH
from config.const import TEMPLATE_FILE_PATH
from config.exception import UnsupportedFileFormatException
from out.csv import create_template
from out.message_producer import parse_and_send_table

bot = Bot(BOT_API_TOKEN)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class File(StatesGroup):
    start = State()
    load_file = State()


@dp.message_handler(commands=['start'])
async def handle_start(message: Message):
    await File.start.set()
    if message.from_user.username not in ADMIN_USERNAMES:
        return await bot.send_message(message.chat.id, 'У вас нет прав администратора')
    await bot.send_message(message.chat.id, 'Выберите действие', reply_markup=start_keyboard)


@dp.callback_query_handler(lambda c: c.data == 'user_table', state=File.start)
async def handle_load_file(callback: CallbackQuery):
    await File.load_file.set()
    return await bot.send_message(callback.message.chat.id, 'Отправьте таблицу с пользователями в формате csv')


@dp.callback_query_handler(lambda c: c.data == 'template', state=File.start)
async def handle_template(callback: CallbackQuery, state: FSMContext):
    create_template()
    await state.finish()
    return await bot.send_document(callback.message.chat.id, open(TEMPLATE_FILE_PATH))


@dp.message_handler(content_types=ContentType.DOCUMENT, state=File.load_file)
async def handle_doc(message: Message, state: FSMContext):
    document: Document = message.document
    if document is not None and is_file_name_valid(document.file_name):

        await document.download(destination_file=USER_TABLE_FILE_PATH)
        try:
            await parse_and_send_table(USER_TABLE_FILE_PATH)
        except UnsupportedFileFormatException:
            return await bot.send_message(message.chat.id,
                                          'Неверное форматирование таблицы. Отправьте исправленный вариант')

        await state.finish()
        return await bot.send_message(message.chat.id,
                                      'Файл загружен успешно, права доступа пользователей скоро будут обновлены')
    await bot.send_message(message.chat.id, 'Неверный тип файла, отправьте таблицу в формате csv')


def is_file_name_valid(file_name: str):
    if file_name.split('.')[-1] == 'csv':
        return True
    return False
