from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

user_table_button = InlineKeyboardButton(text='Ввести таблицу', callback_data='user_table')
get_template_button = InlineKeyboardButton(text='Текущий шаблон', callback_data='template')
start_keyboard = InlineKeyboardMarkup()
start_keyboard.add(user_table_button)
start_keyboard.add(get_template_button)
