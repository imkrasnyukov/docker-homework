import os

from dotenv import load_dotenv

load_dotenv()

PG_USERNAME = os.getenv('PG_USERNAME')
PG_PASSWORD = os.getenv('PG_PASSWORD')
DB_URI = os.getenv('DB_URI')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')

RABBITMQ_USER = os.getenv('RABBITMQ_USER')
RABBITMQ_PASS = os.getenv('RABBITMQ_PASS')
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT')

BOT_API_TOKEN = os.getenv('BOT_API_TOKEN')
ADMIN_USERNAMES = ['imkrasnyukov', 'AccNFR']

ROOT_DIR = '/'.join(__file__.split('/')[:-2])

USER_TABLE_FILE_PATH = ROOT_DIR + '/resources/user_table.csv'
TEMPLATE_FILE_PATH = ROOT_DIR + '/resources/template.csv'
