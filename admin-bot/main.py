import asyncio
import logging

from bot.bot import dp

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


async def main():
    await dp.skip_updates()
    await dp.start_polling()


if __name__ == '__main__':
    asyncio.run(main())
