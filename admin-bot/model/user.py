import uuid
from typing import Optional, Sequence

from sqlalchemy import UUID, VARCHAR, BOOLEAN, select
from sqlalchemy.orm import Mapped, mapped_column, Session

from config.db_base import Base, engine


class User(Base):
    __tablename__ = 'account'

    id: Mapped[uuid.UUID] = mapped_column(UUID, primary_key=True)
    username: Mapped[str] = mapped_column(VARCHAR(255))
    chat_id: Mapped[Optional[str]] = mapped_column(VARCHAR(255))
    is_notifications_active: Mapped[Optional[bool]]
    is_enabled: Mapped[bool] = mapped_column(BOOLEAN, default=False)

    def __repr__(self) -> str:
        return f'User[id={self.id}, username={self.username}, chat_id={self.chat_id},' \
               f' is_notifications_active={self.is_notifications_active}, is_enabled={self.is_enabled}'


def get_all() -> Sequence[User]:
    with Session(engine) as session:
        stmt = select(User)
        users = session.scalars(stmt).all()
        return users


if __name__ == '__main__':
    print(get_all())
