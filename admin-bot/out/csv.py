import csv
from typing import IO, Sequence

from config.const import TEMPLATE_FILE_PATH
from config.exception import UnsupportedFileFormatException
from model.user import get_all, User


def parse_csv(csv_file: IO) -> list:
    result: list = []
    user_reader = csv.reader(csv_file)
    for row in user_reader:
        if len(row) != 1 \
                or not row[0].isascii():
            raise UnsupportedFileFormatException
        result.append({
            'username': row[0]
        })
    return result


def create_template():
    with open(TEMPLATE_FILE_PATH, 'w') as csv_file:
        users: Sequence[User] = get_all()
        usernames: list[str] = []
        for user in users:
            usernames.append(user.username + '\n')
        csv_file.writelines(usernames)
