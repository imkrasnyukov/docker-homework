import asyncio
import json
import logging

import aio_pika

from config.const import RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_HOST, RABBITMQ_PORT
from out.csv import parse_csv

log = logging.getLogger(__name__)


async def parse_and_send_table(file_path: str):
    connection = await aio_pika.connect_robust(
        f'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:{RABBITMQ_PORT}'
    )

    async with connection:
        routing_key = 'user_data'
        channel = await connection.channel()
        await channel.declare_queue(routing_key, durable=True)

        user_list: list
        with open(file_path, 'r') as csv_file:
            user_list = parse_csv(csv_file)

        user_list_serialized = json.dumps(user_list)

        await channel.default_exchange.publish(
            aio_pika.Message(body=user_list_serialized.encode()),
            routing_key=routing_key
        )
        log.info('Message published')
