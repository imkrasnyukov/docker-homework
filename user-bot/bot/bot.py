import logging

from aiogram import Bot, Dispatcher
from aiogram import types

from bot.middleware import AuthMiddleware
from config.const import BOT_API_TOKEN
from config.const import MAX_UPPER_BOUND
from config.exception import ParsingException
from model import user

log = logging.getLogger(__name__)

bot = Bot(token=BOT_API_TOKEN)

dp = Dispatcher(bot)


@dp.message_handler(commands=['start'])
async def handle_start(message: types.Message):
    await bot.send_message(chat_id=message.chat.id,
                           text=f'<b>Задайте шаблон в формате</b>\n'
                                f'<code>Время жизни от: [число от 0]\n'
                                f'Время жизни до: [число до {MAX_UPPER_BOUND}]\n'
                                f'Минимальная ликвидность: [любое число от 0]\n'
                                f'Ссылки dextools: [да/нет]\n'
                                f'Минимальный score токенсниффер: [число от 0 до 100]</code>',
                           parse_mode="HTML")


@dp.message_handler(content_types=types.ContentType.TEXT)
async def handle_template(message: types.Message):
    try:
        load_template(message)
    except ParsingException as e:
        return await bot.send_message(message.chat.id, str(e))
    return await bot.send_message(message.chat.id, 'Значение успешно установлено\n'
                                                   'Ожидайте уведомлений')


def load_data(username: str, chat_id: int, time_lower_bound: int, time_upper_bound: int, liquidity: int,
              is_links_required: bool, tokensniffer_score: int):
    user.set_params(username,
                    chat_id,
                    time_lower_bound,
                    time_upper_bound,
                    liquidity,
                    is_links_required,
                    tokensniffer_score)


def str_to_bool(s: str) -> bool:
    return s == 'да'


def load_template(message: types.Message):
    template = message.text
    lines: list[str] = template.splitlines()
    if len(lines) != 5:
        raise ParsingException('Неверный шаблон')
    lower_bound_line = split_line(lines, 0)
    upper_bound_line = split_line(lines, 1)
    liquidity_line = split_line(lines, 2)
    links_line = split_line(lines, 3)
    tokensniffer_line = split_line(lines, 4)
    if len(lower_bound_line) != 2 or len(upper_bound_line) != 2 \
            or len(liquidity_line) != 2 or len(links_line) != 2 or len(tokensniffer_line) != 2:
        raise ParsingException('Неверный шаблон')

    if lower_bound_line[0] != 'Время жизни от' or upper_bound_line[0] != 'Время жизни до' \
            or liquidity_line[0] != 'Минимальная ликвидность' \
            or links_line[0] != 'Ссылки dextools' or tokensniffer_line[0] != 'Минимальный score токенсниффер':
        raise ParsingException('Неверный шаблон')

    if not lower_bound_line[1].isdecimal() or int(liquidity_line[1]) < 0:
        raise ParsingException('Неверное значение нижней границы времени жизни, введите число')
    if not upper_bound_line[1].isdecimal() or int(upper_bound_line[1]) < 0:
        raise ParsingException('Неверное значение верхней границы времени жизни, введите число')
    if int(upper_bound_line[1]) > MAX_UPPER_BOUND:
        raise ParsingException(f'Значение верхней границы должно быть меньше {MAX_UPPER_BOUND}')
    if not liquidity_line[1].isdecimal() or int(liquidity_line[1]) < 0:
        raise ParsingException('Неверное значение ликвидности, введите число')
    if links_line[1] not in ['да', 'нет']:
        raise ParsingException('Неверное значение ссылок dextools, введите "да" или "нет"')
    if not tokensniffer_line[1].isdecimal() and not 0 < int(tokensniffer_line[1]) <= 100:
        raise ParsingException('Неверное значение score токенсниффера, введите значение от 0 до 100')

    load_data(username=message.from_user.username,
              chat_id=message.chat.id,
              time_lower_bound=int(lower_bound_line[1]),
              time_upper_bound=int(upper_bound_line[1]),
              liquidity=None if int(liquidity_line[1]) == 0 else int(liquidity_line[1]),
              is_links_required=str_to_bool(links_line[1]),
              tokensniffer_score=None if int(tokensniffer_line[1]) == 0 else int(tokensniffer_line[1]))


def split_line(lines: list[str], i: int):
    return list(map(str.strip, lines[i].split(':')))


dp.setup_middleware(AuthMiddleware())
