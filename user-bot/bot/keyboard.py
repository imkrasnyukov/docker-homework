from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

change_filters_button = InlineKeyboardButton('Изменить фильтры', callback_data='change_filters')
start_keyboard = InlineKeyboardMarkup()
start_keyboard.add(change_filters_button)

links_yes_button = InlineKeyboardButton('Да', callback_data='links_yes')
links_no_button = InlineKeyboardButton('Нет', callback_data='links_no')
dextools_links_keyboard = InlineKeyboardMarkup()
dextools_links_keyboard.add(links_yes_button)
dextools_links_keyboard.add(links_no_button)
