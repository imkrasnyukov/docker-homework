from aiogram import types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware

from model.user import is_enabled


class AuthMiddleware(BaseMiddleware):
    async def on_pre_process_message(self, message: types.Message, data):
        username = message.from_user.username
        if not is_enabled(username):
            await message.reply('У Вас нет доступа к боту\n'
                                'Запросите доступ у администратора')
            raise CancelHandler()

    async def on_pre_process_callback_query(self, callback_query: types.CallbackQuery, data):
        username = callback_query.from_user.username
        if not is_enabled(username):
            await callback_query.message.reply('У Вас нет доступа к боту\n'
                                               'Запросите доступ у администратора')
            raise CancelHandler()
