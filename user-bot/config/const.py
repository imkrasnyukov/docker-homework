from os import getenv

from dotenv import load_dotenv

load_dotenv()

PG_USERNAME = getenv('PG_USERNAME')
PG_PASSWORD = getenv('PG_PASSWORD')
DB_URI = getenv('DB_URI')
DB_PORT = getenv('DB_PORT')
DB_NAME = getenv('DB_NAME')

RABBITMQ_USER = getenv('RABBITMQ_USER')
RABBITMQ_PASS = getenv('RABBITMQ_PASS')
RABBITMQ_HOST = getenv('RABBITMQ_HOST')
RABBITMQ_PORT = getenv('RABBITMQ_PORT')

BOT_API_TOKEN = getenv('BOT_API_TOKEN')

MAX_UPPER_BOUND = int(getenv('MAX_UPPER_BOUND'))

SCORE_SELECTOR = "div.Home_content__2fqOz > div:nth-child(4) > div:nth-child(1) > table:nth-child(1) > " \
                 "tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > h2:nth-child(1) > span:nth-child(1)"
