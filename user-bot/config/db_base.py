from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase

from config.const import PG_USERNAME, PG_PASSWORD, DB_URI, DB_PORT, DB_NAME

engine = create_engine(f'postgresql://{PG_USERNAME}:{PG_PASSWORD}@{DB_URI}:{DB_PORT}/{DB_NAME}')


class Base(DeclarativeBase):
    pass
