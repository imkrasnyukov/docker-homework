class TokenNotFoundException(Exception):
    pass


class ParsingException(Exception):
    pass
