import logging

from config.db_base import Base
from config.db_base import engine

# noinspection PyUnresolvedReferences
from model.user import User, Filter


log = logging.getLogger(__name__)


def init_db():
    Base.metadata.create_all(engine)
    log.info('Database initialized')
