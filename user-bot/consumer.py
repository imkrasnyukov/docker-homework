import asyncio
import json
import logging
from typing import Type
from uuid import uuid4

from aio_pika import connect_robust
from aio_pika.abc import AbstractIncomingMessage
from sqlalchemy.orm import Session

from config.const import RABBITMQ_USER, RABBITMQ_PASS, RABBITMQ_HOST, RABBITMQ_PORT
from config.db_base import engine
from model.user import User

log = logging.getLogger(__name__)


def update_users(users: list[dict]):
    with Session(engine) as session:
        existing_users: list[Type[User]] = session.query(User).all()
        existing_users_dict: dict[str, User] = {user.username: user for user in existing_users}

        for user_data in users:
            username = user_data['username']
            if username in existing_users_dict:
                existing_user = existing_users_dict[username]
                if not existing_user.is_enabled:
                    existing_user.is_enabled = True
            else:
                new_user = User(id=uuid4(),
                                username=username,
                                is_enabled=True)
                session.add(new_user)

        for existing_user in existing_users:
            if existing_user.username not in [user_data['username'] for user_data in users]:
                existing_user.is_enabled = False

        session.commit()
        log.info('User table updated')


async def on_message(message: AbstractIncomingMessage) -> None:
    async with message.process():
        message_body = message.body.decode()
        user_list: list[dict] = json.loads(message_body)
        update_users(user_list)


async def rabbitmq_consumer():
    connection = await connect_robust(f"amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@{RABBITMQ_HOST}:{RABBITMQ_PORT}")

    async with connection:
        channel = await connection.channel()
        await channel.set_qos(prefetch_count=1)

        queue = await channel.declare_queue('user_data', durable=True)

        await queue.consume(on_message)
        await asyncio.Future()
