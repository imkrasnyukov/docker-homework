import asyncio
import logging

from bot.bot import dp
from config.init_db import init_db
from consumer import rabbitmq_consumer
from monitoring.notifier import update_and_notify
from monitoring.token_tracker import TokenTracker

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


async def main() -> None:
    await dp.skip_updates()
    await asyncio.gather(dp.start_polling(),
                         rabbitmq_consumer(),
                         update_and_notify(TokenTracker()))


if __name__ == "__main__":
    init_db()
    asyncio.run(main())
