from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class Token:
    id: str
    name: str
    liquidity: Optional[int]
    listed_since: datetime
    is_links_present: bool
    tokensniffer_score: Optional[int]

    def __hash__(self) -> int:
        return hash((self.id, self.name))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Token):
            return NotImplemented
        return self.id == other.id

