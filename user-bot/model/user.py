import logging
import uuid
from typing import Optional, Sequence

from sqlalchemy import ForeignKey
from sqlalchemy import UUID, BOOLEAN, VARCHAR
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.orm import Session

from config.db_base import Base, engine

log = logging.getLogger(__name__)


class User(Base):
    __tablename__ = 'account'

    id: Mapped[uuid.UUID] = mapped_column(UUID, primary_key=True)
    username: Mapped[str] = mapped_column(VARCHAR(255))
    chat_id: Mapped[Optional[int]] = mapped_column(VARCHAR(255))
    is_notifications_active: Mapped[Optional[bool]]
    is_enabled: Mapped[bool] = mapped_column(BOOLEAN, default=False)
    filter: Mapped['Filter'] = relationship(back_populates='user', cascade="all, delete-orphan")


class Filter(Base):
    __tablename__ = 'filter'

    id: Mapped[uuid.UUID] = mapped_column(UUID, primary_key=True)
    user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey('account.id'))
    user: Mapped['User'] = relationship(back_populates='filter')
    time_lower_bound: Mapped[Optional[int]]
    time_upper_bound: Mapped[Optional[int]]
    liquidity: Mapped[Optional[int]]
    is_links_required: Mapped[Optional[bool]]
    tokensniffer_score: Mapped[Optional[int]]


def find_by_username(username: str) -> Optional[User]:
    with Session(engine) as session:
        stmt = select(User).where(User.username == username)
        return session.scalar(stmt)


def is_enabled(username: str) -> bool:
    user = find_by_username(username)
    if user is not None:
        return user.is_enabled
    return False


def set_params(username: str, chat_id: int, time_lower_bound: int,
               time_upper_bound: int, liquidity: int, is_links_required: bool, tokensniffer_score: int):
    with Session(engine) as session:
        user_stmt = select(User).where(User.username == username)
        user: User = session.scalar(user_stmt)
        user.chat_id = chat_id

        filter_: Filter = user.filter
        if filter_ is None:
            filter_ = Filter()
            filter_.user = user
            session.add(filter_)

        filter_.id = uuid.uuid4()
        filter_.time_lower_bound = time_lower_bound
        filter_.time_upper_bound = time_upper_bound
        filter_.liquidity = liquidity
        filter_.is_links_required = is_links_required
        filter_.tokensniffer_score = tokensniffer_score

        session.commit()


def get_upper_bound():
    with Session(engine) as session:
        return session.query(func.max(Filter.time_upper_bound)).scalar() or 0


def get_lower_bound():
    with Session(engine) as session:
        return session.query(func.min(Filter.time_lower_bound)).scalar() or 0


def get_all() -> Sequence[User]:
    with Session(engine) as session:
        stmt = select(User)
        users = session.scalars(stmt).all()
        return users
