import asyncio
import logging

from aiohttp import ClientSession
from aiohttp_retry import RetryClient, ExponentialRetry

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

tokens_uri = 'https://www.dextools.io/chain-ethereum/api/generic/pools?range=1'

headers = {
    'Referer': 'https://www.dextools.io/app/en/ether/pool-explorer',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 '
                  'Safari/537.36',
}


async def get_tokens() -> dict:
    async with ClientSession(headers=headers) as session:
        retry_client = RetryClient(client_session=session)
        retry_options = ExponentialRetry(attempts=5)
        async with retry_client.get(tokens_uri, retry_options=retry_options) as resp:
            log.info(f'Fetched dextools API for tokens retrieval. Code: {resp.status}')
            if resp.status == 200:
                resp_text = await resp.json()
                return resp_text.get('data')
            else:
                log.error(f'Failed to retrieve tokens data. Code: {resp.status}. Error: {resp.reason}')
                raise IOError('Failed to retrieve tokens data.')


async def get_pair_info(pair_id: str) -> dict:
    async with ClientSession(headers=headers) as session:
        retry_client = RetryClient(client_session=session)
        retry_options = ExponentialRetry(attempts=5)
        async with retry_client.get(
                f'https://www.dextools.io/shared/data/pair?address={pair_id}&chain=ether&audit=true',
                retry_options=retry_options) as resp:
            log.debug(f'Fetched dextools API for token introspection. Code: {resp.status}')
            if resp.status == 200:
                resp_text = await resp.json()
                return resp_text.get('data')
            else:
                log.error(f'Failed to introspect token. Code: {resp.status}. Error: {resp.reason}')
                raise IOError('Failed to introspect token.')


def extract_token_id(token_info: dict) -> str:
    token_index = token_info.get('tokenIndex')
    return token_info.get('token' + str(token_index)).get('id')


def extract_pair_id(token_info: dict) -> str:
    return token_info.get('id')

async def main():
    print(await get_pair_info('0x138fc75627a3ca73abc0ba07225e1cc52ac15a8a'))


if __name__ == '__main__':
    asyncio.run(main())