import logging
from datetime import datetime, timezone
from uuid import UUID

import time
from sqlalchemy import select
from sqlalchemy.orm import Session

from bot.bot import bot
from config.db_base import engine
from model.token import Token
from model.user import Filter
from model.user import User
from monitoring.token_tracker import TokenTracker

log = logging.getLogger(__name__)

# dict with user id as key and set of token ids as value
tokens_sent_to_user: dict[UUID, set] = {}


async def notify_on_updates(updated_tokens: set[Token]):
    with Session(engine) as session:
        stmt = select(User)
        users = session.scalars(stmt).all()
        for user in users:
            filter_: Filter = user.filter
            if filter_ is None or not user.is_enabled:
                continue
            now = datetime.utcnow()
            upper_bound = now.replace(hour=now.hour - filter_.time_upper_bound, tzinfo=timezone.utc)  # time less
            lower_bound = now.replace(hour=now.hour - filter_.time_lower_bound, tzinfo=timezone.utc)  # time more
            for token in updated_tokens:
                if tokens_sent_to_user.get(user.id) is None:
                    tokens_sent_to_user[user.id] = set()
                if token.id in tokens_sent_to_user.get(user.id):
                    continue
                if token.listed_since > lower_bound or token.listed_since < upper_bound:
                    continue
                if filter_.tokensniffer_score:
                    if not token.tokensniffer_score or filter_.tokensniffer_score > token.tokensniffer_score:
                        continue
                if filter_.is_links_required and not token.is_links_present:
                    continue
                if filter_.liquidity:
                    if token.liquidity is None or filter_.liquidity > token.liquidity:
                        continue

                text = f'<code>{token.id}</code>\n' \
                       f'Name: <code>{token.name}</code>\n' \
                       f'Liquidity: <code>{token.liquidity}</code>\n' \
                       f'Links: <code>{token.is_links_present}</code>\n' \
                       f'Tokensniffer: <code>{token.tokensniffer_score}</code>\n\n' \
                       f'https://www.dextools.io/app/en/ether/pair-explorer/{token.id}'
                await bot.send_message(user.chat_id, text, parse_mode='HTML')
                tokens_sent_to_user[user.id].add(token.id)


async def update_and_notify(tracker: TokenTracker):
    while True:
        try:
            start = time.time()
            await tracker.update_tokens()
            log.info(f'Time to update tokens: {time.time() - start}')
            updated_tokens = tracker.find_changed_and_new_tokens()
            await notify_on_updates(updated_tokens)
        except Exception as e:
            log.error('Failed to update or notify' + str(e))
            await bot.send_message(5483267990, str(e))
