import logging
import random
from datetime import datetime, timezone, timedelta
from typing import Set, Optional

import time
from selenium.common.exceptions import NoSuchElementException, TimeoutException

import model.user as user
from config.exception import TokenNotFoundException
from model.token import Token
from monitoring.dextools import get_tokens, extract_token_id, extract_pair_id, get_pair_info
from monitoring.tokensniffer import get_token_score

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

random.seed(time.time())


async def extract_created_at(token_data):
    return datetime.fromtimestamp(token_data.get('createdAtTimestamp'), timezone.utc)


async def get_tokens_creates():
    return (await get_tokens()).get('creates')


async def get_bounds():
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    upper_bound = now - timedelta(hours=user.get_upper_bound())
    log.info('upper_bound=' + str(upper_bound))
    lower_bound = now - timedelta(hours=user.get_lower_bound())
    log.info('lower_bound=' + str(lower_bound))
    return lower_bound, upper_bound


async def load_data(token_data):
    token_id = extract_token_id(token_data)
    pair_id = extract_pair_id(token_data)
    pair_info: dict = (await get_pair_info(pair_id))[0]
    liquidity = pair_info.get('metrics').get('liquidity')
    links = pair_info.get('token').get('links')
    is_links_present = any(value != '' for value in links.values())
    token_index = token_data.get('tokenIndex')
    token_name = token_data.get(f'token{token_index}').get('symbol')

    return is_links_present, liquidity, token_id, token_name


class TokenTracker:
    def __init__(self):
        self.previous_tokens = {}
        self.current_tokens = {}
        self.tokensniffer_cache = {}
        self.is_first_run = True

    async def update_tokens(self):
        lower_bound, upper_bound = await get_bounds()
        self.current_tokens.clear()

        tokens: list[dict] = await get_tokens_creates()
        for token_data in tokens:
            created_at = await extract_created_at(token_data)
            if created_at > lower_bound:
                continue
            elif created_at < upper_bound:
                break

            is_links_present, liquidity, token_id, token_name = await load_data(token_data)

            previous_token: Token = self.previous_tokens.get(token_id)
            tokensniffer_score: Optional[int] = previous_token.tokensniffer_score if\
                previous_token is not None else None
            if self.is_first_run:
                await self.add_cache_timeout(token_id)

            now = datetime.utcnow().replace(tzinfo=timezone.utc)
            if self.tokensniffer_cache.get(token_id) is None \
                    or now >= self.tokensniffer_cache.get(token_id):
                try:
                    tokensniffer_score = await get_token_score(token_id)
                except (TimeoutException, NoSuchElementException, TokenNotFoundException) as e:
                    log.warning(e)
                    tokensniffer_score = None

                await self.add_cache_timeout(token_id)

            token = Token(id=token_id,
                          name=token_name,
                          liquidity=liquidity,
                          listed_since=created_at,
                          is_links_present=is_links_present,
                          tokensniffer_score=tokensniffer_score)
            self.current_tokens[token_id] = token
        self.is_first_run = False

    async def add_cache_timeout(self, token_id):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        delay = random.randint(1, 30)
        delay_time = now + timedelta(minutes=delay)
        self.tokensniffer_cache[token_id] = delay_time

    def find_changed_and_new_tokens(self) -> Set[Token]:
        # Find new tokens in the current set that are not in the previous set
        new_tokens = set(self.current_tokens.values()) - set(self.previous_tokens.values())

        previous_tokens_len = len(self.previous_tokens)

        changed_tokens = set()
        for current_token_id in self.current_tokens.keys():
            if previous_token := self.previous_tokens.get(current_token_id):
                current_token = self.current_tokens.get(current_token_id)
                if current_token.name != previous_token.name \
                        or current_token.liquidity != previous_token.liquidity \
                        or current_token.listed_since != previous_token.listed_since \
                        or current_token.tokensniffer_score != previous_token.tokensniffer_score:
                    changed_tokens.add(current_token)

        for token in new_tokens:
            self.previous_tokens[token.id] = token

        # This line provides a cold run of the check for the first time so that the bot does not throw too many
        # notifications
        if previous_tokens_len == 0:
            return set()

        return new_tokens.union(changed_tokens)
