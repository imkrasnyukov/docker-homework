import asyncio
import logging
import random

import time
import undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from config.const import SCORE_SELECTOR
from config.exception import TokenNotFoundException

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

uri = "https://tokensniffer.com/token/eth/"


class DriverStorage:
    def __init__(self, proxy_list: list[str], retries: int) -> None:
        assert len(proxy_list) != 0, 'proxy list length equals 0'
        self.proxy_list: list[str] = proxy_list
        self.retries: int = retries
        self.current_retries: int = 0
        self.driver: uc.Chrome = self._new_driver()

    def _new_driver(self) -> uc.Chrome:
        proxy = random.choice(self.proxy_list)
        options_ = uc.ChromeOptions()
        options_.add_argument(f'--proxy-server={proxy}')
        options_.add_argument('--headless')
        options_.add_argument('--no-sandbox')
        log.info(f'New driver created with proxy: {proxy}')
        return uc.Chrome(options=options_)

    def get_driver(self) -> uc.Chrome:
        if self.current_retries >= self.retries:
            self.current_retries = 0
            self.driver.close()
            self.driver = self._new_driver()
            log.info(f'New driver created. Current retries: {self.current_retries}')
        self.current_retries += 1
        log.info(f'Old driver used. Current retries: {self.current_retries}')
        return self.driver


proxies = [
    '149.126.239.69:9515',
    '185.191.142.71:9355',
    '185.184.77.49:9095',
    '185.148.24.107:9500',
    '192.144.7.110:9845',
    '185.128.215.98:9818',
    '185.147.130.139:9221',
    '185.128.213.51:9184',
    '193.233.170.53:9628',
    '185.148.27.195:9518',
    '192.144.7.159:9044'
]

driver_storage = DriverStorage(proxy_list=proxies, retries=5)


async def get_token_score(token_id: str) -> int:
    driver_ = driver_storage.get_driver()
    token_uri = uri + token_id
    driver_.get(token_uri)
    if not is_token_found(driver_):
        raise TokenNotFoundException

    timeout: int = 10
    if driver_storage.current_retries == 1:
        timeout = 20

    await asyncio.sleep(timeout)

    score = driver_.find_element(By.CSS_SELECTOR, SCORE_SELECTOR)
    log.info(f"score = {extract_score(score.text)}")
    return extract_score(score.text)


def is_token_found(driver_: uc.Chrome) -> bool:
    if "Token not found" in driver_.page_source:
        return False
    else:
        return True


def extract_score(raw_score: str) -> int:
    return int(raw_score.split("/")[0])


if __name__ == '__main__':
    options = uc.ChromeOptions()
    options.add_argument('--proxy-server=185.184.77.49:9095')
    driver = uc.Chrome()
    driver.get('https://tokensniffer.com/')
    time.sleep(100)
